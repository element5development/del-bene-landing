<?php

/*-----------------------------------------
  CORRECT TAB INDEX ON FORMS
-----------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*-----------------------------------------
  EABLE LABEL VISIBILITY OPTION
-----------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*-----------------------------------------
  SUBMIT INPUT TO BUTTON ELEMENT
-----------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button is-secondary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );