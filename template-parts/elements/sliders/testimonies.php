<section class="testimonies-block">
	<h2>Why Detroit's Best Chefs Choose Us</h2>
	<div class="testimonies">
		<?php 
			query_posts(array( 
				'post_type' => 'testimony',
				'showposts' => -1
			));  
		?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="testimony">
				<blockquote>
					<p><?php the_field('quote'); ?></p>
					<h3><?php the_field('name'); ?></h3>
					<h4><?php the_field('company'); ?></h4>
				</blockquote>
			</div>
		<?php endwhile;?>
		<?php wp_reset_query(); ?>
	</div>
</section>