<div id="fullscreen">
	<div class="section contents we-feed">
		<div class="block-contain">
			<div class="overlay"></div>
			<div class="block">
				<h1>We Feed Memories.</h1>
				<p>We equip the top chefs in Michigan with superior produce, elusive ingredients, and personal attention.</p>
				<a href="<?php echo get_home_url(); ?>/contact" class="button is-secondary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section contents why-del">
		<div class="block-contain">
			<div class="block">
				<h2>WHY
					<br/>DEL BENE?</h2>
				<p>Del Bene Produce is committed to delivering distinctive premium products and unmatched customer service to local chefs.
					We focus on building relationships with our customers that goes past “just filling an order”, We help our customers
					grow by helping plan menus, managing food costs, and integrating tech-friendly solutions.</p>
				<a href="<?php echo get_home_url(); ?>/contact" class="button is-tertiary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section contents choose-del">
		<div class="overlay left"></div>
		<div class="overlay right"></div>
		<div class="block-contain">
			<div class="block">
				<h3>DETROIT’S FINEST RESTAURANTS</h3>
				<h2>CHOOSE DEL BENE</h2>
				<div class="companies">
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
				</div>
				<a href="<?php echo get_home_url(); ?>/contact" class="button is-primary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section contents in-season">
		<div class="block-contain">
			<div class="block">
				<h2>WHAT’S IN SEASON?</h2>
				<a target="_blank" href="<?php the_field('market_report'); ?>" class="button is-primary is-ghost">READ THE MARKET REPORT</a>
				<a target="_blank" href="<?php the_field('michigan_farms_list'); ?>" class="button is-primary is-ghost in-line">Michigan Farms List</a>
				<a target="_blank" href="<?php the_field('michigan_farmers_map'); ?>" class="button is-primary is-ghost in-line">Michigan Farmers Map</a>
				<div class="item">
					<svg width="87" height="73" viewBox="0 0 87 73">
						<g fill="none" fill-rule="evenodd" opacity=".66">
							<path d="M61.776 60.534c-3.976 0-6.9-.902-8.47-1.386-1.177-.364-2.14-1.185-2.713-2.313a4.926 4.926 0 0 1-.26-3.815c.787-2.284 1.07-3.264 1.17-3.653a2.448 2.448 0 0 1 .704-1.742 2.487 2.487 0 0 1 3.065-.373c1.614.997 1.137 2.788.958 3.462-.213.797-.623 2.08-1.22 3.817 1.438.44 3.719 1.09 6.766 1.09 3.049 0 5.329-.65 6.766-1.09-1.758-5.147-1.724-8.687-1.722-8.835a2.457 2.457 0 0 1 2.456-2.42h.038a2.457 2.457 0 0 1 2.42 2.495c0 .018 0 2.935 1.485 7.249a4.924 4.924 0 0 1-.26 3.815c-.572 1.128-1.535 1.95-2.712 2.313-1.57.484-4.494 1.386-8.47 1.386"
							  fill="#D5AF89" />
							<path d="M61.45 48.19c-.728 0-1.146-.003-2.009-.053a2.457 2.457 0 0 1 .29-4.906c.775.046 1.075.047 1.869.046h.176c7.146 0 19.132-1.636 19.132-12.587 0-4.602-2.045-9.299-5.61-12.885-3.707-3.73-8.51-5.786-13.522-5.786-2.89 0-4.05.14-6.16 1.069a2.458 2.458 0 0 1-1.978-4.498c2.905-1.278 4.779-1.484 8.138-1.484 12.81 0 24.045 11.02 24.045 23.584 0 5.919-2.59 10.644-7.49 13.666-4.066 2.509-9.79 3.834-16.555 3.834H61.45z"
							  fill="#D5AF89" />
							<path d="M33.898 5.62c-7.602 0-14.874 2.945-20.475 8.293-5.354 5.113-8.424 11.809-8.424 18.373 0 6.085 2.681 10.74 7.968 13.836 4.967 2.907 12.205 4.444 20.931 4.444 8.727 0 15.965-1.537 20.932-4.444 5.287-3.096 7.968-7.75 7.968-13.836 0-6.564-3.07-13.26-8.424-18.373C48.773 8.565 41.5 5.62 33.898 5.62m0 49.86C12.41 55.48.085 47.025.085 32.286c0-7.9 3.625-15.892 9.945-21.926C16.55 4.134 25.026.706 33.898.706c8.873 0 17.35 3.428 23.87 9.654 6.32 6.034 9.943 14.026 9.943 21.926 0 14.74-12.324 23.193-33.813 23.193"
							  fill="#F5C683" />
							<path d="M33.898 72.495c-5.59 0-9.722-1.21-11.941-1.861a5.507 5.507 0 0 1-3.345-2.795 5.705 5.705 0 0 1-.27-4.508c2.216-6.103 2.207-10.237 2.206-10.278a2.461 2.461 0 0 1 2.426-2.483 2.455 2.455 0 0 1 2.487 2.412c.004.201.05 4.999-2.5 12.025a.776.776 0 0 0 .034.611.567.567 0 0 0 .343.3c1.983.582 5.675 1.663 10.56 1.663 4.886 0 8.578-1.081 10.56-1.662a.568.568 0 0 0 .344-.3.776.776 0 0 0 .034-.612c-2.55-7.026-2.504-11.824-2.5-12.025a2.464 2.464 0 0 1 2.496-2.416 2.457 2.457 0 0 1 2.416 2.496c0 .032-.01 4.166 2.206 10.269a5.705 5.705 0 0 1-.269 4.508 5.508 5.508 0 0 1-3.345 2.795c-2.22.65-6.351 1.86-11.942 1.86"
							  fill="#F5C683" />
						</g>
					</svg>
					<h3>MOREL MUSHROOMS</h3>
					<p>Adds nutty, woody flavors to dishes. A premium ingredient that all chefs enjoy using. Available from Northern California,
						Oregon, and locally from foragers in Michigan.</p>
					<a href="#" data-featherlight="#order-modal" class="button is-secondary trigger-order-modal">Order Online</a>
				</div>
				<div class="item">
					<svg width="82" height="106" viewBox="0 0 82 106">
						<defs>
							<linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="abc">
								<stop stop-color="#AEC97C" offset="0%" />
								<stop stop-color="#E52F58" offset="71.317%" />
							</linearGradient>
						</defs>
						<g fill="none" fill-rule="evenodd">
							<g opacity=".66">
								<path d="M18.673 7.389c.337 3.856 1.556 10.945 5.96 14.315 2.435 1.864 5.55 2.347 9.495 1.464.964-3.925.546-7.05-1.267-9.524-3.277-4.472-10.34-5.84-14.188-6.255m11.88 21.183c-3.424 0-6.423-.976-8.961-2.923-8.18-6.275-8.036-20.377-8.026-20.975a2.49 2.49 0 0 1 2.49-2.448h.01c.597.002 14.7.153 20.803 8.46 2.975 4.05 3.53 9.188 1.65 15.272a2.492 2.492 0 0 1-1.693 1.659c-2.225.637-4.318.955-6.273.955"
								  fill="#57B14C" />
								<path d="M37.743 38.762a2.49 2.49 0 0 1-2.485-2.355c-.507-9.359-1.294-11.844-8.485-16.897a2.49 2.49 0 1 1 2.864-4.075c9.041 6.353 10.041 10.493 10.595 20.702a2.49 2.49 0 0 1-2.49 2.625"
								  fill="#83AD38" />
								<path d="M48.384 21.064c4.613 2.403 8.773 2.839 12.383 1.293 4.595-1.967 7.753-6.828 9.592-10.56a44.008 44.008 0 0 0 2.225-5.429c-4.537-.713-14.037-1.495-19.648 3.12-3.03 2.49-4.558 6.381-4.552 11.576m7.892 7.197c-3.665 0-7.544-1.187-11.6-3.557a2.49 2.49 0 0 1-1.23-2.027c-.376-7.55 1.757-13.286 6.34-17.047 9.478-7.78 25.877-3.778 26.571-3.604a2.49 2.49 0 0 1 1.817 2.99c-.165.696-4.173 17.094-15.447 21.92a16.24 16.24 0 0 1-6.451 1.325"
								  fill="#57B14C" />
								<path d="M40.721 37.575a2.49 2.49 0 0 1-2.377-3.236c4.015-12.794 10.179-19.323 22.744-24.09a2.492 2.492 0 0 1 1.766 4.657c-11.196 4.248-16.181 9.527-19.757 20.923a2.492 2.492 0 0 1-2.376 1.746"
								  fill="#83AD38" />
								<path d="M60.685 34.423c-.047 3.166.812 5.495 2.61 7.093 3.045 2.707 8.472 2.91 12.064 2.602-1.124-3.426-3.441-8.337-7.126-10.076-2.175-1.027-4.654-.904-7.548.381m12.112 14.784c-4.03 0-9.18-.75-12.797-3.956-3.272-2.9-4.687-7.152-4.204-12.637a2.491 2.491 0 0 1 1.307-1.978c4.855-2.595 9.321-2.962 13.273-1.09 8.04 3.81 10.465 15.66 10.564 16.163a2.49 2.49 0 0 1-1.928 2.92c-.23.047-2.827.578-6.215.578"
								  fill="#57B14C" />
								<path d="M66.342 40.423a2.481 2.481 0 0 1-1.73-.7c-5.77-5.576-12.484-6.466-19.985-2.66-1.17.594-2.599.21-3.28-.91a2.495 2.495 0 0 1 1.01-3.522c3.936-1.997 7.81-2.96 11.539-2.865 5.136.13 9.88 2.251 14.101 6.303 1.025.984 1.092 2.681.066 3.664-.48.46-1.1.69-1.72.69"
								  fill="#83AD38" />
							</g>
							<path d="M20.316 97.72c-.026.674 1.834 1.778 4.497 1.778s4.523-1.104 4.497-1.779l-2.413-62.7a1.36 1.36 0 0 0-.221-.152c-.442-.251-1.117-.414-1.863-.414-.747 0-1.42.163-1.863.414a1.36 1.36 0 0 0-.22.151L20.315 97.72zm4.497 5.778c-4.627 0-8.631-2.377-8.494-5.933l2.42-62.875c.1-2.635 2.922-4.237 6.074-4.237 3.152 0 5.973 1.602 6.074 4.237l2.42 62.875c.137 3.556-3.867 5.933-8.494 5.933z"
							  fill="url(#abc)" fill-rule="nonzero" transform="rotate(24 24.813 66.976)" />
						</g>
					</svg>
					<h3>RHUBARB</h3>
					<p>Tart and tangy, a favorite for pie but also a great addition throughout your spring menu. The dark- to greenish-red
						stalks add a vibrant color. Available from Michigan growers – a great local flavor.</p>
					<a href="#" data-featherlight="#order-modal" class="button is-secondary trigger-order-modal">Order Online</a>
				</div>
			</div>
		</div>
	</div>
	<div class="section contents team-del">
		<div class="block-contain">
			<div class="overlay left"></div>
			<div class="block">
				<!-- <img src="http://via.placeholder.com/250x250?text=TEAM" alt="Del Bene Team" /> -->
				<h2>MEET OUR TEAM</h2>
				<p>Del Bene Produce was started by Anthony, Fred and Tom Del Bene on Russell Street in the heart of Detroit’s Eastern Market
					in 1988. The company started as one truck delivering to a few restaurants and today we service nearly 500 restaurants,
					institutions and other foodservice customers. We have since moved to a bigger, newer building and now have a fleet of
					twenty vehicles making deliveries six days per week. However, we remain committed to the values that started Del Bene
					Produce – delivering quality products; providing exemplary services; and building longterm relationships.</p>
				<a href="<?php echo get_home_url(); ?>/contact" class="button is-tertiary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section fp-auto-height">
		<?php get_template_part('template-parts/elements/sliders/testimonies'); ?>
		<?php get_template_part('template-parts/footer/footer'); ?>
	</div>
</div>