var $ = jQuery;

$(document).ready(function() {
  /*------------------------------------------------------------------
  	INPUT ADDING AND REMVOING CLASSES
  ------------------------------------------------------------------*/
  $('input:not([type=checkbox]):not([type=radio])').focus(function() {
    $(this).addClass('is-activated');
  });
  $('textarea').focus(function() {
    $(this).addClass('is-activated');
  });
  $('select').focus(function() {
    $(this).addClass('is-activated');
  });
  /*------------------------------------------------------------------
  	ORDER MODAL ENTRANCE ANIMATION
	------------------------------------------------------------------*/
  $('.trigger-order-modal').on('click', function() {
    $('.featherlight-content').addClass('fadeInUp');
  });
  /*------------------------------------------------------------------
  	TESTIMONY COVERFLOW
	------------------------------------------------------------------*/
  $('.testimonies-block').flipster({
    itemContainer: '.testimonies',
    itemSelector: '.testimony',
    loop: 1,
    start: 'center',
    style: 'infinite-carousel',
    spacing: 0,
    scrollwheel: false,
    touch: true,
    buttons: false,
  });
  /*------------------------------------------------------------------
  	FULL SCREEN SLIDER
	------------------------------------------------------------------*/
  $('#fullscreen').fullpage({
    navigation: true,
    navigationPosition: 'left',
	});
	/*------------------------------------------------------------------
  	CLIENTS
	------------------------------------------------------------------*/
	var imgArray = [
		"/wp-content/themes/starting-point/dist/images/client_chartreuse.jpg",
		"/wp-content/themes/starting-point/dist/images/client_delaware.jpg",
		"/wp-content/themes/starting-point/dist/images/client_hotel.jpg",
		"/wp-content/themes/starting-point/dist/images/client_johnny.jpg",
		"/wp-content/themes/starting-point/dist/images/client_mabel.jpg",
		"/wp-content/themes/starting-point/dist/images/client_midfield.jpg",
		"/wp-content/themes/starting-point/dist/images/client_selden.jpg",
		"/wp-content/themes/starting-point/dist/images/client_testabarra.jpg",
		"/wp-content/themes/starting-point/dist/images/client_WAB.jpg",
		"/wp-content/themes/starting-point/dist/images/client_wright.jpg",
		"/wp-content/themes/starting-point/dist/images/client_fishbones.jpg"
	];
	function loadImages() {
		$('.company').each(function(i) {
			var newImage = imgArray.splice(0,1)[0];
			$(this).find('.swap').attr('src', newImage);
		});
	}
	function swapImage() {
		var imgChoice = Math.floor(Math.random() * imgArray.length);
		var newImage = imgArray.splice(imgChoice,1)[0];
		var target = Math.floor(Math.random() * $('.company').length);
		var target = target + 1;
		var mainImg = $('.company:nth-child(' + target + ') .swap').eq(1);
		var swapImg = $('.company:nth-child(' + target + ') .swap').eq(0);
		swapImg.show().attr('src', newImage);
		imgArray.push(mainImg.attr('src'));
		swapImg.fadeIn(2000);
		mainImg.fadeOut(2000, function() {
			swapImg.insertAfter(mainImg);
			swapImage();
		});
	}
	loadImages();
	swapImage();

});